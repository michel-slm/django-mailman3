# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-04-18 16:07+0000\n"
"PO-Revision-Date: 2023-03-12 14:41+0000\n"
"Last-Translator: ssantos <ssantos@web.de>\n"
"Language-Team: Portuguese <https://hosted.weblate.org/projects/gnu-mailman/"
"django-mailman3/pt/>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.16.2-dev\n"

#: forms.py:32
msgid "Username"
msgstr "Utilizador"

#: forms.py:33
msgid "First name"
msgstr "Primeiro nome"

#: forms.py:34
msgid "Last name"
msgstr "Apelido"

#: forms.py:36
msgid "Time zone"
msgstr "Fuso horário"

#: forms.py:43
msgid "A user with that username already exists."
msgstr "Já existe um utilizador com esse nome."

#: templates/account/email.html:6
#: templates/django_mailman3/profile/base.html:17
msgid "Account"
msgstr "Conta"

#: templates/account/email.html:11
msgid "The following e-mail addresses are associated with your account:"
msgstr "Os seguintes endereços de e-mail estão associados à sua conta:"

#: templates/account/email.html:25
msgid "Verified"
msgstr "Verificado"

#: templates/account/email.html:27
msgid "Unverified"
msgstr "Não verificado"

#: templates/account/email.html:29
msgid "Primary"
msgstr "Primário"

#: templates/account/email.html:35
msgid "Make Primary"
msgstr "Tornar primário"

#: templates/account/email.html:36
msgid "Re-send Verification"
msgstr "Reenviar verificação"

#: templates/account/email.html:37 templates/socialaccount/connections.html:34
msgid "Remove"
msgstr "Remover"

#: templates/account/email.html:44
msgid "Warning:"
msgstr "Aviso:"

#: templates/account/email.html:44
msgid ""
"You currently do not have any e-mail address set up. You should really add "
"an e-mail address so you can receive notifications, reset your password, etc."
msgstr ""
"Ainda não configurou um endereço de e-mail. Deve adicionar um endereço de e-"
"mail para receber notificações, redefinir a sua palavra-passe etc."

#: templates/account/email.html:49
msgid "Add E-mail Address"
msgstr "Adicionar endereço de e-mail"

#: templates/account/email.html:55
msgid "Add E-mail"
msgstr "Adicionar e-mail"

#: templates/account/email.html:66
msgid "Do you really want to remove the selected e-mail address?"
msgstr "Deseja remover o endereço de e-mail selecionado?"

#: templates/account/email_confirm.html:6
#: templates/account/email_confirm.html:10
msgid "Confirm E-mail Address"
msgstr "Confirmar endereço de e-mail"

#: templates/account/email_confirm.html:16
#, python-format
msgid ""
"Please confirm that <a href=\"mailto:%(email)s\">%(email)s</a> is an e-mail "
"address for user %(user_display)s."
msgstr ""
"Por favor, confirme que <a href=\"mailto:%(email)s\">%(email)s</a> é um "
"endereço de e-mail do utilizador %(user_display)s."

#: templates/account/email_confirm.html:20
msgid "Confirm"
msgstr "Confirmar"

#: templates/account/email_confirm.html:27
#, python-format
msgid ""
"This e-mail confirmation link expired or is invalid. Please <a href="
"\"%(email_url)s\">issue a new e-mail confirmation request</a>."
msgstr ""
"Esta ligação de confirmação de e-mail expirou ou é inválida. Por favor <a "
"href=\"%(email_url)s\">emita uma nova solicitação de confirmação por e-mail</"
"a>."

#: templates/account/login.html:7 templates/account/login.html:11
#: templates/account/login.html:59
msgid "Sign In"
msgstr "Entrar"

#: templates/account/login.html:18
#, python-format
msgid ""
"Please sign in with one\n"
"of your existing third party accounts. Or, <a href=\"%(signup_url)s\">sign "
"up</a>\n"
"for a %(site_name)s account and sign in below:"
msgstr ""
"Entre com uma das suas\n"
"contas de terceiros existentes. Ou <a href=\"%(signup_url)s\">inscreva-se</"
"a>\n"
"para uma conta do %(site_name)s e entre abaixo:"

#: templates/account/login.html:22
#, python-format
msgid ""
"If you have a %(site_name)s\n"
"account that you haven't yet linked to any third party account, you must "
"log\n"
"in with your account's email address and password and once logged in, you "
"can\n"
"link your third party accounts via the Account Connections tab on your user\n"
"profile page."
msgstr ""

#: templates/account/login.html:28 templates/account/signup.html:17
#, python-format
msgid ""
"If you do not have a\n"
"%(site_name)s account but you have one of these third party accounts, you "
"can\n"
"create a %(site_name)s account and sign-in in one step by clicking the "
"third\n"
"party account."
msgstr ""

#: templates/account/login.html:41 templates/account/signup.html:30
#: templates/django_mailman3/profile/profile.html:72
msgid "or"
msgstr "ou"

#: templates/account/login.html:48
#, python-format
msgid ""
"If you have not created an account yet, then please\n"
"<a href=\"%(signup_url)s\">sign up</a> first."
msgstr ""
"Se ainda não criou uma conta, por favor,\n"
"<a href=\"%(signup_url)s\">registe-se</a> primeiro."

#: templates/account/login.html:61
msgid "Forgot Password?"
msgstr "Esqueceu-se da palavra-passe?"

#: templates/account/logout.html:5 templates/account/logout.html:8
#: templates/account/logout.html:17
msgid "Sign Out"
msgstr "Sair"

#: templates/account/logout.html:10
msgid "Are you sure you want to sign out?"
msgstr "Tem certeza que deseja sair?"

#: templates/account/password_change.html:12
#: templates/account/password_reset_from_key.html:6
#: templates/account/password_reset_from_key.html:9
#: templates/django_mailman3/profile/base.html:20
msgid "Change Password"
msgstr "Alterar palavra-passe"

#: templates/account/password_reset.html:7
#: templates/account/password_reset.html:11
msgid "Password Reset"
msgstr "Redefinição de palavra-passe"

#: templates/account/password_reset.html:16
msgid ""
"Forgotten your password? Enter your e-mail address below, and we'll send you "
"an e-mail allowing you to reset it."
msgstr ""
"Esqueceu-se da sua palavra-passe? Digite o seu endereço de e-mail abaixo e "
"enviaremos um e-mail para que possa redefini-la."

#: templates/account/password_reset.html:22
msgid "Reset My Password"
msgstr "Redefinir minha palavra-passe"

#: templates/account/password_reset.html:27
msgid "Please contact us if you have any trouble resetting your password."
msgstr ""
"Entre em contato conosco se tiver algum problema para redefinir a sua "
"palavra-passe."

#: templates/account/password_reset_from_key.html:9
msgid "Bad Token"
msgstr "Token ruim"

#: templates/account/password_reset_from_key.html:13
#, python-format
msgid ""
"The password reset link was invalid, possibly because it has already been "
"used.  Please request a <a href=\"%(passwd_reset_url)s\">new password reset</"
"a>."
msgstr ""
"A ligação de redefinição de palavra-passe era inválido, possivelmente porque "
"já foi usado. Solicite <a href=\"%(passwd_reset_url)s\">nova redefinição de "
"palavra-passe</a>."

#: templates/account/password_reset_from_key.html:20
msgid "change password"
msgstr "alterar palavra-passe"

#: templates/account/password_reset_from_key.html:25
msgid "Your password is now changed."
msgstr "A sua palavra-passe foi alterada."

#: templates/account/password_set.html:12
msgid "Set Password"
msgstr "Definir palavra-passe"

#: templates/account/signup.html:7 templates/socialaccount/signup.html:6
msgid "Signup"
msgstr "Registar-se"

#: templates/account/signup.html:10 templates/account/signup.html:42
#: templates/socialaccount/signup.html:9 templates/socialaccount/signup.html:21
msgid "Sign Up"
msgstr "Registar"

#: templates/account/signup.html:12
#, python-format
msgid ""
"Already have an account? Then please <a href=\"%(login_url)s\">sign in</a>."
msgstr "já tem uma conta? Então <a href=\"%(login_url)s\">entre</a>."

#: templates/django_mailman3/paginator/pagination.html:45
msgid "Jump to page:"
msgstr "Pular para página:"

#: templates/django_mailman3/paginator/pagination.html:64
msgid "Results per page:"
msgstr "Resultados por página:"

#: templates/django_mailman3/paginator/pagination.html:80
#: templates/django_mailman3/profile/profile.html:71
msgid "Update"
msgstr "Atualizar"

#: templates/django_mailman3/profile/base.html:6
msgid "User Profile"
msgstr "Perfil do Utilizador"

#: templates/django_mailman3/profile/base.html:13
msgid "User profile"
msgstr "Perfil do utilizador"

#: templates/django_mailman3/profile/base.html:13
msgid "for"
msgstr "para"

#: templates/django_mailman3/profile/base.html:23
msgid "E-mail Addresses"
msgstr "Endereços de e-mail"

#: templates/django_mailman3/profile/base.html:30
msgid "Account Connections"
msgstr "Conexões da conta"

#: templates/django_mailman3/profile/base.html:35
#: templates/django_mailman3/profile/delete_profile.html:16
msgid "Delete Account"
msgstr "Apagar conta"

#: templates/django_mailman3/profile/delete_profile.html:11
msgid ""
"Are you sure you want to delete your account? This will remove your account "
"along with all your subscriptions."
msgstr ""
"Tem certeza que deseja apagar a sua conta? Isso removerá a sua conta com "
"todas as suas inscrições."

#: templates/django_mailman3/profile/profile.html:20
#: templates/django_mailman3/profile/profile.html:57
msgid "Edit on"
msgstr "Editar em"

#: templates/django_mailman3/profile/profile.html:28
msgid "Primary email:"
msgstr "E-mail primário:"

#: templates/django_mailman3/profile/profile.html:34
msgid "Other emails:"
msgstr "Outros e-mails:"

#: templates/django_mailman3/profile/profile.html:40
msgid "(no other email)"
msgstr "(nenhum outro e-mail)"

#: templates/django_mailman3/profile/profile.html:45
msgid "Link another address"
msgstr "Vincular outro endereço"

#: templates/django_mailman3/profile/profile.html:53
msgid "Avatar:"
msgstr "Avatar:"

#: templates/django_mailman3/profile/profile.html:63
msgid "Joined on:"
msgstr "Juntou-se em:"

#: templates/django_mailman3/profile/profile.html:72
msgid "cancel"
msgstr "cancelar"

#: templates/openid/login.html:10
msgid "OpenID Sign In"
msgstr "Entrar com OpenID"

#: templates/socialaccount/connections.html:9
msgid ""
"You can sign in to your account using any of the following third party "
"accounts:"
msgstr ""
"Pode entrar na sua conta a usar qualquer uma das seguintes contas de "
"terceiros:"

#: templates/socialaccount/connections.html:42
msgid ""
"You currently have no social network accounts connected to this account."
msgstr "Atualmente, não tem contas de redes sociais conectadas a esta conta."

#: templates/socialaccount/connections.html:45
msgid "Add a 3rd Party Account"
msgstr "Adicionar uma conta de terceiros"

#: templates/socialaccount/signup.html:11
#, python-format
msgid ""
"You are about to use your %(provider_name)s account to login to\n"
"%(site_name)s. As a final step, please complete the following form:"
msgstr ""
"Usará a sua a conta %(provider_name)s para entrar em\n"
"%(site_name)s. Como etapa final, preencha o seguinte formulário:"

#: templatetags/pagination.py:43
msgid "Newer"
msgstr "Mais novos"

#: templatetags/pagination.py:44
msgid "Older"
msgstr "Mais velhos"

#: templatetags/pagination.py:46
msgid "Previous"
msgstr "Anterior"

#: templatetags/pagination.py:47
msgid "Next"
msgstr "Próximo"

#: views/profile.py:72
msgid "The profile was successfully updated."
msgstr "O perfil foi atualizado com sucesso."

#: views/profile.py:74
msgid "No change detected."
msgstr "Nenhuma mudança detectada."

#: views/profile.py:110
msgid "Successfully deleted account"
msgstr "Conta apagada com êxito"
